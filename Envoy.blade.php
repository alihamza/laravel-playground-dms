@servers(['staging' => 'forge@13.237.105.16', 'local' => '127.0.0.1'])

@task('deploy-staging', ['on' => 'staging'])
cd ~/laravel-playground.staging.iddigital.com.au/
git pull origin master
composer install --no-interaction
sudo -S service php7.4-fpm reload
php artisan migrate --force
php artisan cache:clear
@endtask
