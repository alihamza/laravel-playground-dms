<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Core\Persistence\Db\Connection\IConnection;
use Dms\Core\Persistence\Db\Mapping\IOrm;
use Dms\Core\Persistence\DbRepository;
use App\Domain\Services\Persistence\ICourseRepository;
use App\Domain\Entities\Course;

/**
 * The database repository implementation for the App\Domain\Entities\Course entity.
 */
class DbCourseRepository extends DbRepository implements ICourseRepository
{
    public function __construct(IConnection $connection, IOrm $orm)
    {
        parent::__construct($connection, $orm->getEntityMapper(Course::class));
    }
}