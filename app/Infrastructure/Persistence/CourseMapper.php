<?php declare(strict_types = 1);

namespace App\Infrastructure\Persistence;

use Dms\Common\Structure\FileSystem\Persistence\ImageMapper;
use Dms\Core\Exception\InvalidArgumentException;
use Dms\Core\Exception\InvalidOperationException;
use Dms\Core\Persistence\Db\Mapping\Definition\MapperDefinition;
use Dms\Core\Persistence\Db\Mapping\EntityMapper;
use App\Domain\Entities\Course;

/**
 * The App\Domain\Entities\Course entity mapper.
 */
class CourseMapper extends EntityMapper
{
    /**
     * Defines the entity mapper
     *
     * @param MapperDefinition $map
     *
     * @return void
     * @throws InvalidArgumentException
     * @throws InvalidOperationException
     */
    protected function define(MapperDefinition $map)
    {
        $map->type(Course::class);
        $map->toTable('courses');

        $map->idToPrimaryKey('id');

        $map->property(Course::NAME)->to('name')->asVarchar(255);

        $map->embeddedCollection(Course::IMAGES)
            ->toTable('course_images')
            ->withPrimaryKey('id')
            ->withForeignKeyToParentAs('course_id')
            ->using(new ImageMapper('images', 'images_file_name', public_path('app/courses')));
    }
}
