<?php declare(strict_types = 1);

namespace App\Cms\Modules;

use Dms\Common\Structure\FileSystem\Image;
use Dms\Core\Auth\IAuthSystem;
use Dms\Core\Common\Crud\CrudModule;
use Dms\Core\Common\Crud\Definition\CrudModuleDefinition;
use Dms\Core\Common\Crud\Definition\Form\CrudFormDefinition;
use Dms\Core\Common\Crud\Definition\Table\SummaryTableDefinition;
use App\Domain\Services\Persistence\ICourseRepository;
use App\Domain\Entities\Course;
use Dms\Common\Structure\Field;
use Dms\Core\Form\Builder\Form;
use Illuminate\Support\Str;

/**
 * The course module.
 */
class CourseModule extends CrudModule
{


    public function __construct(ICourseRepository $dataSource, IAuthSystem $authSystem)
    {

        parent::__construct($dataSource, $authSystem);
    }

    /**
     * Defines the structure of this module.
     *
     * @param CrudModuleDefinition $module
     */
    protected function defineCrudModule(CrudModuleDefinition $module)
    {
        $module->name('courses');

        $module->labelObjects()->fromProperty(Course::NAME);

        $module->metadata([
            'icon' => 'book'
        ]);

        $module->crudForm(function (CrudFormDefinition $form) {
            $form->section('Details', [
                $form->field(
                    Field::create('name', 'Name')->string()->required()
                )->bindToProperty(Course::NAME),
                //
                $form->field(
                    Field::create('images', 'Images')->arrayOf(
                        Field::create('image', 'Image')->form(Form::create()->section('', [
                            Field::create('image', 'Images')->image()->required()->moveToPathWithStaticFileNameAndClientExtension(public_path('app/courses'), Str::random())
                        ])->build())->map(function (array $input) {
                            return $input['image'];
                        }, function (Image $image) {
                            return ['image' => $image];
                        }, Image::type())
                    )->mapToCollection(Image::collectionType())
                )->bindToProperty(Course::IMAGES),
            ]);
        });

        $module->removeAction()->deleteFromDataSource();

        $module->summaryTable(function (SummaryTableDefinition $table) {
            $table->mapProperty(Course::NAME)->to(Field::create('name', 'Name')->string()->required());

            $table->view('all', 'All')
                ->loadAll()
                ->asDefault();
        });
    }
}
