<?php declare(strict_types = 1);

namespace App\Cms;

use App\Cms\Modules\CourseModule;
use Dms\Core\Package\Definition\PackageDefinition;
use Dms\Core\Package\Package;
use App\Cms\Modules\TodoItemModule;

/**
 * The App package.
 */
class AppPackage extends Package
{
    /**
     * Defines the structure of this cms package.
     *
     * @param PackageDefinition $package
     *
     * @return void
     */
    protected function define(PackageDefinition $package)
    {
        $package->name('App');

        $package->metadata([
            'icon' => 'cog',
        ]);

        $package->modules([
            'todo-items'    => TodoItemModule::class,
            'courses'       => CourseModule::class,
        ]);
    }
}
