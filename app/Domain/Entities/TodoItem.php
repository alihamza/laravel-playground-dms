<?php declare(strict_types = 1);


namespace App\Domain\Entities;


use Dms\Core\Model\Object\ClassDefinition;
use Dms\Core\Model\Object\Entity;

class TodoItem extends Entity
{
    const DESCRIPTION = 'description';
    const ASSIGNEE = 'assignee';
    const COMPLETED = 'completed';

    public string $description;
    public ?string $assignee = null;
    public bool $completed;

    /**
     * Initialises a new TODO item.
     *
     * @param string $description
     * @param string|null $assignee
     */
    public function __construct(string $description, ?string $assignee = null)
    {
        parent::__construct();
        $this->description = $description;
        $this->assignee = $assignee;
        $this->completed   = false;
    }

    /**
     * Defines the structure of this entity.
     *
     * @param ClassDefinition $class
     */
    protected function defineEntity(ClassDefinition $class)
    {

    }
}
