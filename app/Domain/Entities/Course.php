<?php


namespace App\Domain\Entities;


use Dms\Common\Structure\FileSystem\Image;
use Dms\Core\Model\Collection;
use Dms\Core\Model\Object\ClassDefinition;
use Dms\Core\Model\Object\Entity;

class Course extends Entity
{
    const NAME  = 'name';
    const IMAGES  = 'images';

    /**
     * @var string
     */
    public $name;

    /**
     * @var Collection|Image[]
     */
    public $images;

    protected function defineEntity(ClassDefinition $class)
    {
        $class->property($this->name)->asString();

        $class->property($this->images)->asType(Image::collectionType());
    }
}
