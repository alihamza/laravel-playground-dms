<?php declare(strict_types = 1);

namespace App\Domain\Services\Persistence;

use Dms\Core\Model\ICriteria;
use Dms\Core\Model\ISpecification;
use Dms\Core\Persistence\IRepository;
use App\Domain\Entities\Course;

/**
 * The repository for the App\Domain\Entities\Course entity.
 */
interface ICourseRepository extends IRepository
{
    /**
     * {@inheritDoc}
     *
     * @return Course[]
     */
    public function getAll() : array;

    /**
     * {@inheritDoc}
     *
     * @return Course
     */
    public function get($id);

    /**
     * {@inheritDoc}
     *
     * @return Course[]
     */
    public function getAllById(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Course|null
     */
    public function tryGet($id);

    /**
     * {@inheritDoc}
     *
     * @return Course[]
     */
    public function tryGetAll(array $ids) : array;

    /**
     * {@inheritDoc}
     *
     * @return Course[]
     */
    public function matching(ICriteria $criteria) : array;

    /**
     * {@inheritDoc}
     *
     * @return Course[]
     */
    public function satisfying(ISpecification $specification) : array;
}
