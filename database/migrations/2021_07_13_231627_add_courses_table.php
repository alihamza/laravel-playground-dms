<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->string('name', 255);

        });

        Schema::create('course_images', function (Blueprint $table) {
            $table->integer('id')->autoIncrement()->unsigned();
            $table->integer('course_id')->unsigned();
            $table->string('images', 500);
            $table->string('images_file_name', 255)->nullable();

            $table->index('course_id', 'IDX_4E277F27591CC992');
        });

        Schema::table('course_images', function (Blueprint $table) {
            $table->foreign('course_id', 'fk_course_images_course_id_courses')
                    ->references('id')
                    ->on('courses')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_images');
        Schema::drop('courses');

    }
}
